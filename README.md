# Strategy Pattern
## Why?
### Problem 1 (not the OOP way)
![image.png](./images/image.png)
### Problem 2 (Forcing Implementation & not DRY)
![image-1.png](./images/image-1.png)
## How?
### Implementing Strategy Pattern
![image-2.png](./images/image-2.png)
## What?
### Definition
The Strategy design pattern is a behavioural pattern that allows multiple related algorithms to be passed as parameters to the application during runtime. It allows you to define a family of algorithms, each in its own class but with interchangeable objects.
### Pros and Cons
**Pros**

- The implementation details of the algorithms being used are isolated from the part of the program that uses that algorithm.
- You can swap out algorithms being used at runtime very easily without changing much of the program structure.
- Open/Closed principle: New strategies/ algorithms can be introduced without having to change the context of the application.

**Cons**

- Clients must be aware of all the different algorithms and their unique characteristics to be able to select the one they want to use.
- Functional programming eliminates the need for this pattern in a lot of cases since functions can be used to represent the algorithms and can be called when needed. This functional support also makes the code less ‘bloated’ and might be the preferred approach.

# References

[https://refactoring.guru/design-patterns/strategy](https://refactoring.guru/design-patterns/strategy)

[https://www.digitalocean.com/community/tutorials/strategy-design-pattern-in-java-example-tutorial](https://www.digitalocean.com/community/tutorials/strategy-design-pattern-in-java-example-tutorial)

[https://www.youtube.com/watch?v=QZIvlny1Onk](https://www.youtube.com/watch?v=QZIvlny1Onk)

## Running the Program
`
javac -d bin App.java && java -cp ".;bin" App
`
